 
Project Description
	
	This “face-detection” project accept your image through the laptop camera and detect the face by drawing a rectangle over the face. It is implemented using the ROS environment.
 Installing ROS Kinetic on python 2.7 :
● ROS Kinetic ONLY supports Wily (Ubuntu 15.10), Xenial (Ubuntu 16.04) 
and Jessie (Debian 8) for debian packages.
http://wiki.ros.org/kinetic/Installation/Ubuntu

Installing OpenCV
follow the steps given in the ‘Ubuntu 16.04: How to install OpenCV’ , pyimagesearch
https://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv/

 
install cv_bridge
sudo apt-get install ros-(ROS version name)-cv-bridge

sudo apt-get install ros-(ROS version name)-vision-opencv
Writting simple publisher and subscriber (python)
follow the steps given in the tutorial :
http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29


